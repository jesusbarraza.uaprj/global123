document.addEventListener("DOMContentLoaded", function () {
    const generarCalificacionesBtn = document.getElementById("generarCalificaciones");
    const calificacionesElement = document.getElementById("calificaciones");
    const noAprobadosElement = document.getElementById("noAprobados");
    const aprobadosElement = document.getElementById("aprobados");
    const promedioNoAprobadosElement = document.getElementById("promedioNoAprobados");
    const promedioAprobadosElement = document.getElementById("promedioAprobados");
    const promedioGeneralElement = document.getElementById("promedioGeneral");

    generarCalificacionesBtn.addEventListener("click", () => {
        const calificaciones = [];
        let noAprobados = 0;
        let aprobados = 0;
        let sumNoAprobados = 0;
        let sumAprobados = 0;

        for (let i = 0; i < 35; i++) {
            const calificacion = Math.floor(Math.random() * 11);
            calificaciones.push(calificacion);

            if (calificacion < 7) {
                noAprobados++;
                sumNoAprobados += calificacion;
            } else {
                aprobados++;
                sumAprobados += calificacion;
            }
        }

        const promedioNoAprobados = noAprobados > 0 ? (sumNoAprobados / noAprobados).toFixed(1) : 0;
        const promedioAprobados = aprobados > 0 ? (sumAprobados / aprobados).toFixed(1) : 0;
        const promedioGeneral = (
            (sumNoAprobados + sumAprobados) /
            (noAprobados + aprobados)
        ).toFixed(1);

        calificacionesElement.textContent = `Calificaciones: ${calificaciones.join(", ")}`;
        noAprobadosElement.textContent = `${noAprobados} alumnos No aprobados`;
        aprobadosElement.textContent = `${aprobados} alumnos Aprobados`;
        promedioNoAprobadosElement.textContent = `${promedioNoAprobados} Promedio de no aprobados`;
        promedioAprobadosElement.textContent = `${promedioAprobados} Promedio de Aprobados`;
        promedioGeneralElement.textContent = `${promedioGeneral} Promedio General`;
    });
});
